﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePaddleWASD : MonoBehaviour {

    private Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update () {
        
    }

    public float speed = 10f;

    void FixedUpdate()
    {
        Vector3 pos = rigidbody.position;

        if (Input.GetAxis("Horizontal") > 0)
        {
            pos.x += Input.GetAxis("Horizontal");
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            pos.x += Input.GetAxis("Horizontal");
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            pos.z += Input.GetAxis("Vertical");
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            pos.z += Input.GetAxis("Vertical");
        }

        Vector3 dir = pos - rigidbody.position;
        dir = dir.normalized;
        rigidbody.velocity = dir * speed;
    }

}
